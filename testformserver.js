const express = require('express');
const exphbs = require( 'express-handlebars');
const app = express();
const port = 5601;




app.engine('hbs', exphbs({defaultLayout: 'main', extname: '.hbs'}));
app.set('view engine', 'hbs');

app.get('/', function (req, res) {
  res.render('form');
});

app.get('/form_submit', function (req, res) {
  let message = 'No code provided';
  if (req.query.code) {
    message = `Following code was loaded:${req.query.code}`;
  }
  res.header('x-xss-protection',0);
  res.render('form_submit', { message, code: req.query.code });
});


app.listen(port, () => console.log(`Form app listening on port ${port}!`));
